# Desafio Blue de Seleção QA #

Olá, queremos convidá-lo a participar de nosso desafio de seleção. Pronto para participar? 
Seu trabalho será visto por nosso time e você receberá ao final um feedback sobre o que achamos do seu trabalho. Não é legal? 

Sobre a oportunidade:

A vaga é para Analista de Qualidade de Software, temos vagas com diversos níveis de senioridade e para cada um deles utilizaremos critérios específicos considerando esse aspecto, combinado? 
Se você for aprovado nesta etapa, será convidado para uma entrevista final com nosso time de especialistas..

## Desafio Técnico ##

Nós da Conductor/Ben trabalhamos para transformar a experiência de compra de nossos clientes e nada melhor que uma aplicação confiável. 
Para isso, devemos agilizar nossas entregas e garantir sempre a qualidade das aplicações através de soluções inovadoras e testes automatizados.

Pré-requisito:

* Linguagem de programação java.

Desejável/Diferenciais

* Cucumber (V4+)
* Spring Boot
* Java 8
* jUnit

O que esperamos como escopo mínimo:

* Levantar cenários e especificar os casos de testes;
* Descrever possíveis bugs, realizando o detalhamento dos passos a serem reproduzidos;
* Criar automação com casos de teste para as operações CRUD.

Fluxo de Atividade:

* Consumir API REST (json).

Regras de Negócio:

* Realizar as chamadas nos diversos métodos da API (POST,GET,DELETE...) existentes na plataforma https://reqres.in/ ;
* Utilize seu conhecimento para aplicar as melhores validações (Campos obrigatórios nas requests, conteúdo e status da response...) nas chamadas da API.

## Instruções ##

* Crie um repositório privado no bitbucket para o projeto e adicione como colaborador o usuário **luiz_martins**;
* Desenvolva automação dos testes consumindo a plataforma especificada e envie junto com todos os artefatos já citados até a data prevista;
* Após concluir seu trabalho faça um push;
* Responda o e-mail onde o desafio foi enviado notificando a finalização para validação.

**Utilize todo seu conhecimento e nos mostre seu potencial!**

**Boa sorte!**

